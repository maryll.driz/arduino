const mongoose = require("mongoose")

const cartItemSchema = new mongoose.Schema({
    rfid: String,
    categories: String,
    productName: String,
    price: Number
});

const cartItem = mongoose.model("cartItem", cartItemSchema);

module.exports = cartItem;