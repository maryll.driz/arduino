const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors')
require('dotenv').config();

const productRoute = require("./productRoute")
const app = express();
const PORT = process.env.PORT || 3000;

// MongoDB connection
mongoose.connect(process.env.MONGODB_URI)
    .then(() => console.log('Connected to MongoDB'))
    .catch(err => console.error('Error connecting to MongoDB:', err));

// Middleware
app.use(express.json());
app.use(cors());

// Routes
app.use("/api/item", productRoute)

// Start server
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
