const Product = require("./productModel");

const allProduct = async(req, res) => {
    try {
        const products = await Product.find();
        res.json(products);
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

const oneProduct = async (req, res) => {
    try {
        const { rfid } = req.params;
        const products = await Product.findOne({ rfid });
        res.json(products);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};


const addProduct = async (req, res) => {
    const { rfid, productName, price } = req.body;
    const newItem = new Product({
        rfid,
        productName,
        price
    });
    try {
        const savedItem = await newItem.save();
        res.status(201).json(savedItem);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
};

const updateProduct = async(req, res) => {
    try {
        const { rfid } = req.params;
        const { productName, price } = req.body;

        const updatedProduct = await Product.findOneAndUpdate(
            { rfid },
            { $set: { productName, price } },
            { new: true } 
        );

        if (!updatedProduct) {
            return res.status(404).json({ message: 'Product not found' });
        }

        res.json(updatedProduct);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }         
}

const removeProduct = async (req, res) => {
    try {
        const removedItem = await Product.findOneAndDelete(req.params.rfid);
        if (removedItem) {
            res.json({ message: 'Item removed from cart', removedItem });
        } else {
            res.status(404).json({ message: 'Item not found' });
        }
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

module.exports = {
    allProduct,
    oneProduct,
    addProduct,
    updateProduct,
    removeProduct
};
