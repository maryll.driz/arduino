const express = require("express");
const route = express.Router();

const {
    allProduct,
    oneProduct, 
    addProduct, 
    updateProduct,
    removeProduct
} = require("./productController")

route.get("/all", allProduct)
route.get("/:rfid", oneProduct)
route.post("/add", addProduct)
route.put("/:rfid", updateProduct)
route.delete("/remove", removeProduct)

module.exports = route