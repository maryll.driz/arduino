#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include <Wire.h>
#include <MFRC522.h> // Include the MFRC522 library
#include <DNSServer.h>
#include <LiquidCrystal_I2C.h>

const char* ssid = "GlobeAtHome_D6E09_2.4"; // Router's username
const char* pass = "fBnkD6BE"; // Router's password

constexpr uint8_t RST_PIN = D3;     // Configurable, see typical pin layout above
constexpr uint8_t SS_PIN = D4;      // Configurable, see typical pin layout above
constexpr uint8_t BUZZER_PIN = D8;  // Pin to which the buzzer is connected

MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class
MFRC522::MIFARE_Key key;

LiquidCrystal_I2C lcd(0x27, 16, 2); // Address 0x27, 16 columns, and 2 rows
String tag;

void setup() {
  Serial.begin(9600);
  SPI.begin(); // Initialize SPI bus
  rfid.PCD_Init(); // Init MFRC522
  Wire.begin();  // Add this line
  lcd.init();
  lcd.clear();
  lcd.backlight();
  lcd.print("    WELCOME ");
  lcd.setCursor(0, 1);
  lcd.print("  TO  PUREMART");
  delay(3000);
  lcd.clear();
  delay(10000);

  pinMode(BUZZER_PIN, OUTPUT);  // Set the buzzer pin as an output

  Serial.print("Connecting to ");
  Serial.print(ssid);

  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println(WiFi.localIP());

  Serial.println("Scan RFID Tag!");
  lcd.print("scan rfid tag!");
}

void loop() {
  if (!rfid.PICC_IsNewCardPresent())
    return;
  if (rfid.PICC_ReadCardSerial()) {
    tag = ""; // Clear the tag before reading a new one
    for (byte i = 0; i < 4; i++) {
      tag += String(rfid.uid.uidByte[i], HEX);
    }
    Serial.println(tag);

    lcd.clear(); // Clear the LCD before printing new tag value
    lcd.setCursor(0, 0);
    lcd.print("Tag: ");
    lcd.print(tag);

    digitalWrite(BUZZER_PIN, HIGH); // Turn on the buzzer
    delay(500);  // Buzz for 0.5 seconds
    digitalWrite(BUZZER_PIN, LOW);  // Turn off the buzzer

    delay(2000); // Optional: Display the tag for 2 seconds
    lcd.clear(); // Clear the LCD after displaying

    rfid.PICC_HaltA();
    rfid.PCD_StopCrypto1();
  }
}